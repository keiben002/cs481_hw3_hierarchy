﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

/*
 * I used a source to figure out a slider function on page3.xaml.cs and I used another source to figure out
 * how to change buttons, labels and backgrounds(with their attributes, of course). These are the sources:
 * https://www.geeksforgeeks.org/how-to-set-the-background-color-of-the-button-in-c-sharp/
 * https://docs.microsoft.com/en-us/xamarin/xamarin-forms/user-interface/slider
 * 
 * For the large majority of my code these were used for reference and most of the code I thought up myself
 * but I thought that it would be better to show where I got the formatting and base idea's from.
 */



namespace CS481_HW3_Hierarchy
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }

        //this is the page1 navigation button that will bring the user to a new page1 upon clicking 
        async void Page_1_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new Page1());  //pushes a new page 1 onto the navigation stack
        }

        //this is the page2 navigation button that will bring the user to a new page2 upon clicking
        async void Page_2_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new Page2());    //pushes a new page 2 onto the navigation stack
        }

        //this is the page3 navigation page that will bring the user to a new page3 upon clicking
        async void Page_3_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new Page3());    //pushes a new page 3 onto the navigation stack
        }

        /*This is the appearing function that will trigger upon entering the page, since 
        this is the main page, it will change main page attributes
        */
        async void ContentPage_Appearing(object sender, EventArgs e)
        {
            await Task.Delay(1200);
            Label label = souls;
            Button button1 = artorias;
            Button button2 = nameless;
            Button button3 = cinder;
            souls.TextColor = Color.Turquoise;          //textcolor's are changed to Turquoise upon entering
            artorias.TextColor = Color.Turquoise;
            nameless.TextColor = Color.Turquoise;
            cinder.TextColor = Color.Turquoise;

            souls.FontAttributes = FontAttributes.Italic;       //fontattributes are changed to italic upon entering
            artorias.FontAttributes = FontAttributes.Italic;
            nameless.FontAttributes = FontAttributes.Italic;
            cinder.FontAttributes = FontAttributes.Italic;


        }
        /*
         * This is the disappearing function that will trigger upon exiting from the page,
         * since this the mainpage, an event will trigger upon exiting
         */
        async void ContentPage_Disappearing(object sender, EventArgs e)
        {
            await Task.Delay(1200);
            Label label = souls;
            Button button1 = artorias;
            Button button2 = nameless;
            Button button3 = cinder;
            souls.TextColor = Color.IndianRed;
            artorias.TextColor = Color.IndianRed;       //textcolor's are changed back to indian red upon exiting (the original color)
            nameless.TextColor = Color.IndianRed;
            cinder.TextColor = Color.IndianRed;

            souls.FontAttributes = FontAttributes.Bold;     //fontattributes are changed to bold upon exiting (the original font attribute)
            artorias.FontAttributes = FontAttributes.Bold;
            nameless.FontAttributes = FontAttributes.Bold;
            cinder.FontAttributes = FontAttributes.Bold;
        }
    }
}

