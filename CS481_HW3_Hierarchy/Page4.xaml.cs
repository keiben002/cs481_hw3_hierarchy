﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CS481_HW3_Hierarchy
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Page4 : ContentPage
    {
        public Page4()
        {
            InitializeComponent();
        }

        //this function triggers upon clicking a button that sends the user back to the root(main page)
        async void Double_Pop(object sender, EventArgs e)
        {
            await Navigation.PopToRootAsync();
        }

        //When exiting the page, it will display an alert message that notifies the user that they have left the page
        private void ContentPage_Disappearing(object sender, EventArgs e)
        {
            base.DisplayAlert("Notice!", "You have left Sif's Page", "Next");
        }
    }
}

