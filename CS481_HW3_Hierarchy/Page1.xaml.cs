﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CS481_HW3_Hierarchy
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Page1 : ContentPage
    {
        public Page1()
        {
            InitializeComponent();
        }

        //This onDisappearing function changes the text color of the label Artorias and buttons color_changer, 
        //and sif back to it's original text color which was white.
        private void ContentPage_Disappearing(object sender, EventArgs e)
        {
            base.DisplayAlert("Notice!", "You have left Artorias Page", "Next");

            Button button1 = color_changer;
            Button button2 = sif;
            Label label = Artorias;

            button1.TextColor = Color.White;
            label.TextColor = Color.White;
            button2.TextColor = Color.White;

        }

        /*
         * This on appearing event changes the color_changer button to color "LightSkyBlue" 
         */
        async void ContentPage_Appearing(object sender, EventArgs e)
        {
            await Task.Delay(1200);
            Button button = color_changer;
            button.TextColor = Color.LightSkyBlue;
        }

        //this function/button is for changing all the text colors to the color "Firebrick" upon pressing
        private void color_change(object sender, EventArgs e)
        {
            Label label = Artorias;
            Button button1 = sif;
            Button button2 = color_changer;

            label.TextColor = Color.Firebrick;    //changes label(Artorius) text color to firebrick
            button1.TextColor = Color.Firebrick;  //changes button1(sif) text color to firebrick
            button2.TextColor = Color.Firebrick;

        }

        //when the button is clicked, a new page 4 is pushed onto the navigation stack
        async void Button_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new Page4());    //pushes a new page 2 onto the navigation stack
        }
    }
}


