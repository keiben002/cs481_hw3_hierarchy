﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CS481_HW3_Hierarchy
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Page3 : ContentPage
    {
        public Page3()
        {
            InitializeComponent();
        }

        //When exiting the page, it will display an alert message that notifies the user that they have left the page
        private void ContentPage_Disappearing(object sender, EventArgs e)
        {
            base.DisplayAlert("Notice!", "You have left Soul of Cinder Page", "Next");

        }

        //when entering the page the label of the text will change to white and the attributes will change to bold
        async void ContentPage_Appearing(object sender, EventArgs e)
        {
            await Task.Delay(1200);
            Label label = cinderlabel;
            label.TextColor = Color.White;
            label.FontAttributes = FontAttributes.Bold;
        }

        /*This function controls the value of the slider. In this case the slider controls the opacity of the picture
          so when the value is increased on the slider the picture will become more visible. When the value is decreased
          the opacity of the picture is also decreased.
          
          Source: https://docs.microsoft.com/en-us/xamarin/xamarin-forms/user-interface/slider

          **This is also the source that gave me many ideas on appearing and disappearing but I used my own thoughts and ideas
          for formulating what I wanted to accomplish my task**
       */
        private void Slider_ValueChanged(object sender, ValueChangedEventArgs e)
        {
            double val = e.NewValue;
            cinderimage.Opacity = val;
        }
    }
}


